function spawn_fire_particles(pos)
    minetest.add_particlespawner(40, 0.45, pos, pos,
         {x=-0.7, y=2, z=-0.7}, {x=0.7, y=4.3, z=0.7},
         {x=0.2, y=0.1, z=0.2}, {x=-0.2, y=0.2, z=-0.2},
         0.2, 0.4, 1.6, 2.3, false, "pyromancy_fire_particles.png")
end
