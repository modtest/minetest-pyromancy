minetest.register_node("pyromancy:heat_pipe", {
	description = "Heat Pipe",
	groups = {cracky = 3, stone = 3},
	tiles = {
		"pyromancy_heat_pipe.png",
	},
	drawtype = "nodebox",
	paramtype = "light",
	paramtype2 = "facedir",
	sunlight_propagates = true,
	node_box = {
		type = "connected",
		fixed = {
			{-0.0625, -0.0625, -0.0625, 0.0625, 0, 0.0625}
		},		
		connect_front = {
			{-0.125, -0.125, -0.5625, 0.125, 0.0625, -0.4375},
			{-0.0625, -0.0625, -0.4375, 0.0625, 0, -0.0625},
		},
		connect_left = {
			{-0.5625, -0.125, -0.125, -0.4375, 0.0625, 0.125},
			{-0.4375, -0.0625, -0.0625, 0.0625, 0, 0.0625},
		},
		connect_back = {
            {-0.125, -0.125, 0.4375, 0.125, .0625, 0.5625},
			{-0.0625, -0.0625, 0.0625, 0.0625, 0, 0.4375},
		},
		connect_right = {
			{0.4375, -0.125, -0.125, 0.5625, 0.0625, 0.125},
			{-0.0625, -0.0625, -0.0625, 0.4375, 0, 0.0625},
		},
	},
	connects_to = {"pyromancy:heat_pipe", "pyromancy:fire_sigil", "pyromancy:fuel_container", "pyromancy:pulverizer", "pyromancy:mana_rune"},
})
