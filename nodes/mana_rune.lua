minetest.register_node("pyromancy:mana_rune", {
	description = "Mana Rune",
	paramtype2 = "facedir",
	place_param2 = 0,
	tiles = {
        "pyromancy_mana_rune_top.png",
        "pyromancy_mana_rune_top.png",
        "pyromancy_mana_rune.png",
        "pyromancy_mana_rune.png",
        "pyromancy_mana_rune.png",
        "pyromancy_mana_rune.png",
    },
	is_ground_content = false,
    light_source = 12,
	groups = {picky = 3, oddly_breakable_by_hand = 2},
})

minetest.register_abm{
    nodenames = {"pyromancy:mana_rune"},
    neighbors = {"default:lava_source"},
    interval = 30,
    chance = 3,
    action = function(pos)
        give_sparks("singleplayer", 8)

        spawn_fire_particles(pos)
    end,
}
