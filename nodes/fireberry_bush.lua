minetest.register_node("pyromancy:fireberry_bush", {
    description = "Fireberry Bush",
    groups = {cracky =3, stone = 3},
	tiles = {
		"pyromancy_fireberry_bush_top.png",
		"pyromancy_fireberry_bush_bottom.png",
		"pyromancy_fireberry_bush_side.png",
        "pyromancy_fireberry_bush_side.png",
        "pyromancy_fireberry_bush_side.png",
        "pyromancy_fireberry_bush_side.png",
	},
	drawtype = "nodebox",
	paramtype = "light",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.3125, -0.25, -0.3125, 0.3125, 0.375, 0.3125},
			{-0.125, -0.5, -0.125, 0.125, -0.3125, 0.125},
			{-0.25, 0.375, -0.25, 0.25, 0.4375, 0.25},
			{-0.25, -0.3125, -0.25, 0.25, -0.25, 0.25},
		}
	}
})

