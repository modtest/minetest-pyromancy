minetest.register_node("pyromancy:target", {
	description = "Target",
	groups = {cracky = 3, stone = 3},
	tiles = {
		"pyromancy_target_side.png",
		"pyromancy_target_side.png",
		"pyromancy_target_side.png",
		"pyromancy_target_side.png",
		"pyromancy_target_back.png",
		"pyromancy_target_front.png"
	},
	drawtype = "nodebox",
	paramtype = "light",
	paramtype2 = "facedir",
	sunlight_propagates = true,
	node_box = {
		type = "fixed",
		fixed = {
			{0.3125, -0.4375, 0.375, 0.4375, 0.4375, 0.5}, 
			{-0.4375, -0.4375, 0.375, -0.3125, 0.4375, 0.5},
			{-0.5, 0.4375, 0.3125, 0.5, 0.5, 0.5},
			{-0.375, -0.375, 0.25, 0.375, 0.375, 0.375},
			{-0.5, -0.5, 0.3125, 0.5, -0.4375, 0.5},
			{-0.125, -0.4375, 0.3125, 0.125, 0.4375, 0.5},
		}
	}
})

