minetest.register_node("pyromancy:fire_sigil", {
	description = "Fire Sigil",
	groups = {cracky = 3, stone = 3},
	light_source = 8, 
	tiles = {
		"pyromancy_sigil_top.png",
		"pyromancy_sigil.png",
		"pyromancy_sigil_side.png",
		"pyromancy_sigil_side.png",
		"pyromancy_sigil_side.png",
		"pyromancy_sigil_side.png^pyromancy_sigil_front.png"
	},
	drawtype = "nodebox",
	paramtype = "light",
	paramtype2 = "facedir",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.4375, -0.5, -0.4375, 0.4375, 0.3125, 0.4375},
			{-0.5, -0.5, 0.3125, -0.3125, 0.4375, 0.5},
			{-0.5, -0.5, -0.5, -0.3125, 0.4375, -0.3125},
			{0.3125, -0.5, -0.5, 0.5, 0.4375, -0.3125},
			{0.3125, -0.5, 0.3125, 0.5, 0.4375, 0.5},
			{-0.3125, -0.5, -0.5, 0.3125, -0.25, 0.5},
			{-0.5, 0.125, -0.3125, 0.5, 0.25, 0.3125},
			{-0.3125, 0.125, -0.5, 0.3125, 0.25, 0.5},
			{-0.5, -0.5, -0.3125, 0.5, -0.25, 0.3125},
		}
	},

	on_rightclick = function(pos, node, player, itemstack, pointed_thing)
		if player:get_wielded_item():get_name() == "pyromancy:pyrothermic_lump" then
			fire_sigil_craft(pos, player, itemstack, "pyromancy:pyrothermic_ingot 1")
		end
	end

})

fire_sigil_craft = function(pos, player, itemstack, output)
    	-- Take sparks from player
	if take_sparks(player:get_player_name(), 36) then 

        -- Spawn fire particles
        spawn_fire_particles(pos)

        -- Spawn and drop output item
        minetest.add_item({x=pos.x, y=pos.y + 0.9, z=pos.z}, ItemStack(output))	

        -- Spawn particles when output item hits sigil
        minetest.after(0.36, function()
            pos.y = pos.y + 0.35
            spawn_fire_particles(pos)
        end
        )

        -- Take input item
        if not minetest.setting_getbool("creative_mode") then
            local taken = itemstack:take_item(1)
        end
        return itemstack
    end
end
