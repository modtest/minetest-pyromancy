function draw_hud(player_name)
    local player = minetest.get_player_by_name(player_name)

    local spark_bg = player:hud_add({
        hud_elem_type = "image",
        position = {x = 0.03, y = 0.985},
        scale = {x = 220, y = 30},
        text = "pyromancy_hud_bg.png",
    })
    
    spark_label = player:hud_add({
        hud_elem_type = "text",
        position = {x = 0.08, y = 0.985},
        scale = {x = 100, y = 100},
        text = "Sparks: " .. get_spark_count(player_name),
        number = 0xFFFFFF
    })

    local spark_img = player:hud_add({
        hud_elem_type = "image",
        position = {x = 0.015, y = 0.984},
        scale = {x = 4, y = 4},
        text = "pyromancy_fire_particles.png",
    })

end

function update_hud(player_name)
    local player = minetest.get_player_by_name(player_name)
    player:hud_change(spark_label, "text", "Sparks: " .. get_spark_count(player_name)) 
end

minetest.register_on_joinplayer(function(player)
        local player_name = player:get_player_name()
        draw_hud(player_name)
        update_hud(player_name)
    end
)
