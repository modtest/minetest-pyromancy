minetest.register_node("pyromancy:fuel_container", {
	description = "Fuel Container",
	groups = {cracky = 3, stone = 3},
	tiles = {
		"pyromancy_fuel_container_top.png",
		"pyromancy_fuel_container.png",
		"pyromancy_fuel_container.png",
		"pyromancy_fuel_container.png",
		"pyromancy_fuel_container_back.png",
		"pyromancy_fuel_container.png"
	},
	drawtype = "nodebox",
	paramtype = "light",
	paramtype2 = "facedir",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.4375, -0.375, -0.4375, -0.3125, 0.375, 0.4375},
			{0.3125, -0.375, -0.4375, 0.4375, 0.375, 0.4375},
			{-0.3125, -0.375, 0.3125, 0.3125, 0.375, 0.4375},
			{-0.3125, -0.375, -0.4375, 0.3125, 0.375, -0.3125},
			{-0.375, -0.5, -0.375, 0.375, -0.375, 0.375},
		}
	},
    	selection_box = {
        	type = "fixed",
        	fixed = {
			{-0.4375, -0.375, -0.4375, 0.4375, 0.375, 0.4375},
			{-0.375, -0.5, -0.375, 0.375, -0.375, 0.375},
	},
    }
})

