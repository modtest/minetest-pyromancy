local spark_count = {}

-- TODO: Create and fill file if it does not exist
local function save_sparks()
	local f = io.open(minetest.get_worldpath() .. "/sparks.txt", "w")
	f:write(minetest.serialize(spark_count))
    f:close()
end

local function load_sparks()
	local f = io.open(minetest.get_worldpath() .. "/sparks.txt", "r")

    if f == nil then
        spark_count["singleplayer"] = 25
        save_sparks()        
    else
        spark_count = minetest.deserialize(f:read("*all"))
    end

    f:close()
end

function take_sparks(player_name, count)
    if spark_count[player_name] - count > 0 then
    	spark_count[player_name] = spark_count[player_name] - count
        update_hud(player_name)
        return true
    else
        minetest.chat_send_player(player_name, "Not enough sparks! " .. count - spark_count[player_name] .. " more needed.")
        update_hud(player_name)
        return false
    end
end

function give_sparks(player_name, count)
    spark_count[player_name] = spark_count[player_name] + count
    update_hud(player_name)
end

function get_spark_count(name)
    return spark_count[name]
end

minetest.register_chatcommand("sparks", {
    func = function(name, param)
        if spark_count[name] == nil then
            spark_count[name] = 25
            return true, "You have been given " .. spark_count[name] .. " sparks to get you started."
        else 
            return true, "You have " .. spark_count[name] .. " sparks."
        end
    end,
})

minetest.register_on_shutdown(function()
    save_sparks()
end)

load_sparks()
