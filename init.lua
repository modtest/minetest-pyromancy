local modpath = minetest.get_modpath("pyromancy")

dofile(modpath .. "/hud.lua")
dofile(modpath .. "/particles.lua")
dofile(modpath .. "/recipes.lua")
dofile(modpath .. "/sparks.lua")

-- Nodes
dofile(modpath .. "/nodes/beacon.lua")
dofile(modpath .. "/nodes/fire_sigil.lua")
dofile(modpath .. "/nodes/fuel_container.lua")
dofile(modpath .. "/nodes/heat_pipe.lua")
dofile(modpath .. "/nodes/mana_rune.lua")
dofile(modpath .. "/nodes/target.lua")
dofile(modpath .. "/nodes/fireberry_bush.lua")

-- Items
dofile(modpath .. "/items/paxel.lua")
dofile(modpath .. "/items/pyrothermic_ingot.lua")
dofile(modpath .. "/items/pyrothermic_lump.lua")
dofile(modpath .. "/items/pyrothermic_spear.lua")
dofile(modpath .. "/items/pyromantic_tome.lua")
