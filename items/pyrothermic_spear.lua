minetest.register_craftitem("pyromancy:pyrothermic_spear", {
	description = "Pyrothermic Spear",
	inventory_image = "pyromancy_pyrothermic_spear.png",
	tool_capabilities = {
		full_punch_interval = 0.7,
		max_drop_level=9999,
		groupcaps={
			snappy={times={[1]=2.5, [2]=1.20, [3]=0.35}, uses=45, maxlevel=3},
		},
		damage_groups = {fleshy=6},
	}
})
