minetest.register_craftitem("pyromancy:pyromantic_tome", {
	description = "Book",
	inventory_image = "pyromancy_pyromantic_tome.png",

	on_use = function(itemstack, user, pointed_thing)
       minetest.show_formspec(user:get_player_name(), "pyromancy:pyromantic_tome",
                              "size[4,3]" ..
                                 "label[0,0;Hello, singleplayer]" ..
                                 "field[1,1.5;3,1]" ..
                                 "button_exit[1,2;2,1;exit;Save]")
    end,
})
