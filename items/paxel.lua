minetest.register_craftitem("pyromancy:paxel", {
	description = "Pyrothermic Paxel",
	inventory_image = "pyromancy_paxel.png",

	tool_capabilities = {
        full_punch_interval = 0.9,
        max_drop_level=3,

        groupcaps={
            cracky = {times={[1]=2.0, [2]=1.0, [3]=0.50}, uses=30, maxlevel=3},
            crumbly = {times={[1]=1.10, [2]=0.50, [3]=0.30}, uses=30, maxlevel=3},
            choppy = {times={[1]=2.10, [2]=0.90, [3]=0.50}, uses=30, maxlevel=2},
        },

        damage_groups = {fleshy=5},
    },
})

minetest.register_on_dignode(function(pos, oldnode, digger)
    if digger:get_wielded_item():get_name() == "pyromancy:paxel" then
        spawn_fire_particles(pos)
        take_sparks(digger:get_player_name(), 16)
    end
end)
