minetest.register_node("pyromancy:beacon", {
	description = "Fire Beacon",
	groups = {cracky = 3, stone = 3},
	tiles = {
		"pyromancy_beacon_top.png",
		"pyromancy_beacon_bottom.png",
		"pyromancy_beacon.png",
		"pyromancy_beacon.png",
		"pyromancy_beacon.png",
		"pyromancy_beacon.png",
	},
	drawtype = "nodebox",
	paramtype = "light",
	sunlight_propagates = true,
    light_source = 12,
	node_box = {
		type = "fixed",
		fixed = {
			{-0.0625, -0.375, -0.0625, 0.0625, 0.375, 0.0625},
			{-0.125, -0.5, -0.125, 0.125, -0.375, 0.125},
			{-0.125, 0.125, -0.125, 0.125, 0.1875, 0.125},
			{-0.1875, -0.5, -0.1875, 0.1875, -0.4375, 0.1875},
		}
	}
})

